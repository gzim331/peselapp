//
//  ViewController.swift
//  peselApp
//
//  Created by michal on 17/05/2019.
//  Copyright © 2019 Michal Zimka. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var peselTextField: UITextField!
    @IBOutlet weak var peselLabel: UILabel!
    @IBOutlet weak var peselButton: UIButton!
    @IBOutlet weak var sexLabel: UILabel!
    @IBOutlet weak var birthdayLabel: UILabel!
    
    private var pesel: Pesel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        peselTextField.resignFirstResponder()
    }
    
    @IBAction func peselButton(_ sender: UIButton) {
        
        pesel = Pesel(pesel: peselTextField.text!)
        
        if pesel.peselCorrect() == true && peselTextField.text!.count == 11 {
            peselLabel.text = "Pesel Correct"
            sexLabel.text = pesel.sex()
            birthdayLabel.text = pesel.birthday()
            self.view.backgroundColor = UIColor(red: 38/255, green: 153/255, blue: 43/255, alpha: 1.0)
        } else {
            peselLabel.text = "Pesel Incorrect"
            sexLabel.text = ""
            birthdayLabel.text = ""
            self.view.backgroundColor = UIColor.red
        }
    }
}
