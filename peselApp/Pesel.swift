//
//  Pesel.swift
//  peselApp
//
//  Created by michal on 17/05/2019.
//  Copyright © 2019 Michal Zimka. All rights reserved.
//

import Foundation

class Pesel {
    private var pesel: String = ""
    private var amount: Int = 0;
    
    init(pesel: String) {
        self.pesel = pesel
    }
    
    private func mod(_ amount: Int) -> Int {
        return amount % 10
    }
    
    private func charToInt(_ tab: [Character]) -> [Int] {
        var tablica: [Int] = []
        
        for i in tab {
            tablica.append(Int(String(i))!)
        }
        
        return tablica
    }
    
    private func pstab() -> [Int]{
        return charToInt(Array(pesel))
    }
    
    public func sex() -> String {
        if pstab()[9] % 2 == 0 {
            return "Female"
        } else {
            return "Male"
        }
    }
    
    public func birthday() -> String {
        var age = ""
        
        switch pstab()[2] {
        case 8, 9:
            age = "\(pstab()[4])\(pstab()[5]).\(pstab()[2])\(pstab()[3]).18\(pstab()[0])\(pstab()[1])"
        case 0, 1:
            age = "\(pstab()[4])\(pstab()[5]).\(pstab()[2])\(pstab()[3]).19\(pstab()[0])\(pstab()[1])"
        case 2, 3:
            age = "\(pstab()[4])\(pstab()[5]).\(pstab()[2])\(pstab()[3]).20\(pstab()[0])\(pstab()[1])"
        case 4, 5:
            age = "\(pstab()[4])\(pstab()[5]).\(pstab()[2])\(pstab()[3]).21\(pstab()[0])\(pstab()[1])"
        case 6, 7:
            age = "\(pstab()[4])\(pstab()[5]).\(pstab()[2])\(pstab()[3]).22\(pstab()[0])\(pstab()[1])"
        default:
            break
        }
        
        return age
    }
    
    public func peselCorrect() -> Bool {
        for i in 0..<pstab().count {
            switch i {
            case 0,4,8:
                amount += pstab()[i]*9
            case 1,5,9:
                amount += pstab()[i]*7
            case 2,6:
                amount += pstab()[i]*3
            case 3,7:
                amount += pstab()[i]*1
            default:
                break
            }
        }
        
        if mod(amount) == pstab().last {
            return true
        } else {
            return false
        }
    }
}
